import { IsNumberString, IsString } from 'class-validator';

export class CreateStudentDto {
  @IsString()
  nome: string;

  @IsNumberString()
  matricula: number;
}
