import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Student {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  matricula: number;

  @Column('varchar', { length: 200 })
  nome: string;
}
