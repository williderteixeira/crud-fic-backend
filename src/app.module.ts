import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Professor } from './professor/entities/professor.entity';
import { ProfessorModule } from './professor/professor.module';
import { StudentModule } from './student/student.module';
import { CursoFicModule } from './curso-fic/curso-fic.module';
import { TurmaModule } from './turma/turma.module';
import { Turma } from './turma/entities/turma.entity';
import { CursoFic } from './curso-fic/entities/curso-fic.entity';
import { Student } from './student/entities/student.entity';

@Module({
  imports: [
    CursoFicModule,
    StudentModule,
    ProfessorModule,
    TurmaModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'db',
      port: 5432,
      username: 'default',
      password: 'default',
      database: 'curso-fic',
      entities: [CursoFic, Turma, Student, Professor],
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
