import { IsString, IsDateString } from 'class-validator';
export class CreateTurmaDto {
  id: number;

  @IsString()
  nome: string;

  @IsDateString()
  dataInicio: Date;

  @IsDateString()
  dataFim: Date;
}
