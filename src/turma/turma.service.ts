import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTurmaDto } from './dto/create-turma.dto';
import { UpdateTurmaDto } from './dto/update-turma.dto';
import { Turma } from './entities/turma.entity';

@Injectable()
export class TurmaService {
  constructor(
    @InjectRepository(Turma)
    private turmaRepository: Repository<Turma>,
  ) {}

  async create(createTurmaDto: CreateTurmaDto) {
    const turma = this.turmaRepository.create(createTurmaDto);
    await this.turmaRepository.save(createTurmaDto);
    return turma;
  }

  async findAll(): Promise<Turma[]> {
    return await this.turmaRepository.find();
  }

  async findOne(id: number): Promise<Turma> {
    return await this.turmaRepository.findOne(id);
  }

  async update(id: number, createTurmaDto: CreateTurmaDto) {
    await this.turmaRepository.update(id, createTurmaDto);
    return await this.turmaRepository.findOne(id);
  }

  async remove(id: number) {
    await this.turmaRepository.delete(id);
  }
}
