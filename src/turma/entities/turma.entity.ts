import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Turma {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column('varchar', { length: 200 })
  private nome: string;

  @Column()
  private dataInicio: Date;

  @Column()
  private dataFim: Date;
}
