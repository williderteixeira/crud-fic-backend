import { IsString } from 'class-validator';

export class CreateProfessorDto {
  id: number;
  @IsString()
  nome: string;
}
