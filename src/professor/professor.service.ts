import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Professor } from './entities/professor.entity';
import { CreateProfessorDto } from './dto/create-professor.dto';
import { UpdateProfessorDto } from './dto/update-professor.dto';

@Injectable()
export class ProfessorService {
  constructor(
    @InjectRepository(Professor)
    private professorRepository: Repository<Professor>,
  ) {}

  async create(createProfessorDto: CreateProfessorDto) {
    const professor = this.professorRepository.create(createProfessorDto);
    await this.professorRepository.save(createProfessorDto);
    return professor;
  }

  async findAll(): Promise<Professor[]> {
    return await this.professorRepository.find();
  }

  async findOne(id: number): Promise<Professor> {
    return await this.professorRepository.findOne(id);
  }

  async update(id: number, updateProfessorDto: UpdateProfessorDto) {
    await this.professorRepository.update(id, updateProfessorDto);
    return await this.professorRepository.findOne(id);
  }

  async remove(id: number) {
    await this.professorRepository.delete(id);
  }
}
