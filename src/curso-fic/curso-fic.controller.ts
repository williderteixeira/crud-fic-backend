import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CursoFicService } from './curso-fic.service';
import { CreateCursoFicDto } from './dto/create-curso-fic.dto';
import { UpdateCursoFicDto } from './dto/update-curso-fic.dto';

@Controller('curso-fic')
export class CursoFicController {
  constructor(private readonly cursoFicService: CursoFicService) {}

  @Post()
  create(@Body() createCursoFicDto: CreateCursoFicDto) {
    return this.cursoFicService.create(createCursoFicDto);
  }

  @Get()
  findAll() {
    return this.cursoFicService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.cursoFicService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCursoFicDto: UpdateCursoFicDto,
  ) {
    return this.cursoFicService.update(+id, updateCursoFicDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cursoFicService.remove(+id);
  }
}
