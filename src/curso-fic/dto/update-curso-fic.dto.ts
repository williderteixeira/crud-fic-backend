import { PartialType } from '@nestjs/mapped-types';
import { CreateCursoFicDto } from './create-curso-fic.dto';

export class UpdateCursoFicDto extends PartialType(CreateCursoFicDto) {}
