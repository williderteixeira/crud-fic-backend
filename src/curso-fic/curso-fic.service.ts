import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCursoFicDto } from './dto/create-curso-fic.dto';
import { UpdateCursoFicDto } from './dto/update-curso-fic.dto';
import { CursoFic } from './entities/curso-fic.entity';

@Injectable()
export class CursoFicService {
  constructor(
    @InjectRepository(CursoFic)
    private cursoFicRepository: Repository<CursoFic>,
  ) {}

  async create(createCursoFicDto: CreateCursoFicDto) {
    const cursoFic = this.cursoFicRepository.create(createCursoFicDto);
    await this.cursoFicRepository.save(createCursoFicDto);
    return cursoFic;
  }

  async findAll(): Promise<CursoFic[]> {
    return await this.cursoFicRepository.find();
  }

  async findOne(id: number): Promise<CursoFic> {
    return await this.cursoFicRepository.findOne(id);
  }

  async update(id: number, updateCursoFicDto: UpdateCursoFicDto) {
    await this.cursoFicRepository.update(id, updateCursoFicDto);
    return await this.cursoFicRepository.findOne(id);
  }

  async remove(id: number) {
    await this.cursoFicRepository.delete(id);
  }
}
