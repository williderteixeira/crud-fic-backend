import { Test, TestingModule } from '@nestjs/testing';
import { CursoFicController } from './curso-fic.controller';
import { CursoFicService } from './curso-fic.service';

describe('CursoFicController', () => {
  let controller: CursoFicController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CursoFicController],
      providers: [CursoFicService],
    }).compile();

    controller = module.get<CursoFicController>(CursoFicController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
