import { Test, TestingModule } from '@nestjs/testing';
import { CursoFicService } from './curso-fic.service';

describe('CursoFicService', () => {
  let service: CursoFicService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CursoFicService],
    }).compile();

    service = module.get<CursoFicService>(CursoFicService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
