import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CursoFicService } from './curso-fic.service';
import { CursoFicController } from './curso-fic.controller';
import { CursoFic } from './entities/curso-fic.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CursoFic])],
  controllers: [CursoFicController],
  providers: [CursoFicService],
})
export class CursoFicModule {}
