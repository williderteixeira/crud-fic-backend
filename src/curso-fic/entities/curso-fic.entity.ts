import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class CursoFic {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { length: 200 })
  nome: string;
}
