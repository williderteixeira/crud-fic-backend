FROM node:14.17-alpine

# Create work directory
WORKDIR /app

# Copy dependencies
COPY package*.json ./

RUN apk add --no-cache git

RUN npm install --verbose

RUN npm i -g @nestjs/cli --verbose

COPY . /app

COPY --chown=node:node . /app

USER node

EXPOSE 3333

# Build and run the app
CMD ["npm", "run", "start:dev"]
